/*
    MARK: Задание 1:
    Описать несколько структур – любой легковой автомобиль и любой грузовик. Структуры должны содержать марку авто, год выпуска, объем багажника/кузова, запущен ли двигатель, открыты ли окна, заполненный объем багажника.

    Описать перечисление с возможными действиями с автомобилем: запустить/заглушить двигатель, открыть/закрыть окна, погрузить/выгрузить из кузова/багажника груз определенного объема.

    Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия.

    Инициализировать несколько экземпляров структур. Применить к ним различные действия. Положить объекты структур в словарь как ключи, а их названия как строки например var dict = [structCar: 'structCar'].
*/

enum CarAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadCargo(volume: Double)
    case unloadCargo(volume: Double)
}

struct Car {
    let brand:          String
    let year:           Int
    var trunkVolume:    Double
    var isEngineOn:     Bool
    var areWindowsOpen: Bool
    var loadedVolume:   Double
    
    mutating func performAction(action: CarAction) {
        switch action {
        case .startEngine:
            isEngineOn = true
        case .stopEngine:
            isEngineOn = false
        case .openWindows:
            areWindowsOpen = true
        case .closeWindows:
            areWindowsOpen = false
        case .loadCargo(let volume):
            if (loadedVolume + volume <= trunkVolume) {
                loadedVolume += volume
            } else {
                print("Не удается загрузить груз, недостаточно места в багажнике.")
            }
        case .unloadCargo(let volume):
            if (loadedVolume >= volume) {
                loadedVolume -= volume
            } else {
                print("Не удается выгрузить груз, недостаточно загруженный объем.")
            }
        }
    }
}

struct Truck {
    let brand:          String
    let year:           Int
    var cargoVolume:    Double
    var isEngineOn:     Bool
    var areWindowsOpen: Bool
    var loadedVolume:   Double
    
    mutating func performAction(action: CarAction) {
        switch action {
        case .startEngine:
            isEngineOn = true
        case .stopEngine:
            isEngineOn = false
        case .openWindows:
            areWindowsOpen = true
        case .closeWindows:
            areWindowsOpen = false
        case .loadCargo(let volume):
            if (loadedVolume + volume <= cargoVolume) {
                loadedVolume += volume
            } else {
                print("Не удается загрузить груз, недостаточно места в грузовом отсеке.")
            }
        case .unloadCargo(let volume):
            if (loadedVolume >= volume) {
                loadedVolume -= volume
            } else {
                print("Не удается выгрузить груз, недостаточно загруженный объем.")
            }
        }
    }
}

var car1 = Car(brand: "Honda", year: 2022, trunkVolume: 500, isEngineOn: false, areWindowsOpen: false, loadedVolume: 0)
var car2 = Car(brand: "Toyota", year: 2021, trunkVolume: 300, isEngineOn: true, areWindowsOpen: true, loadedVolume: 200)
var truck1 = Truck(brand: "Volvo", year: 2020, cargoVolume: 10000, isEngineOn: false, areWindowsOpen: false, loadedVolume: 0)

var carDictionary: [String: Car] = ["Машина1": car1, "Машина2": car2]
var truckDictionary: [String: Truck] = ["Грузовик1": truck1]

car1.performAction(action: .startEngine)
car2.performAction(action: .loadCargo(volume: 100))
truck1.performAction(action: .openWindows)

print(carDictionary)
print(truckDictionary)


// MARK: Задание 2:
// Почитать о Capture List (см ссылку ниже) - и описать своими словами и сделать скрин своего примера и объяснения Capture.


// MARK: Задание 3:
// Набрать код который на скриншоте понять в чем там проблема и решить эту проблему.

/*
    Проблема в данном коде заключается в наличии циклических ссылок между объектами классов Car и Man.

    Объект car имеет ссылку на объект man через свойство driver, а объект man в свою очередь имеет ссылку на объект car через свойство myCar. Если мы попытаемся освободить память, установив nil для обоих объектов car и man, то они не будут удалены из памяти, так как они продолжают ссылаться друг на друга, создавая утечку памяти.

    Для решения этой проблемы необходимо разорвать циклические ссылки. Одним из способов это сделать является использование слабых ссылок. В Swift это можно сделать с помощью ключевого слова weak.

    Изменим код, используя слабые ссылки:
*/

class Car2 {
    weak var driver: Man2?
    
    deinit {
        print("машина удалена из памяти")
    }
}

class Man2 {
    weak var myCar: Car2?
    
    deinit {
        print("мужчина удален из памяти")
    }
}

var car: Car2? = Car2()
var man: Man2? = Man2()

car?.driver = man
man?.myCar = car

car = nil
man = nil

// После установки nil на все ссылки, объекты будут освобождены из памяти
// В данном коде мы использовали ключевое слово weak для определения слабых ссылок между объектами Car и Man. Теперь, когда мы устанавливаем nil для объектов car и man, они будут удалены из памяти, так как нет сильных ссылок на них.



// MARK: Задание 4:
// У нас есть класс мужчины и его паспорта. Мужчина может родиться и не иметь паспорта, но паспорт выдается конкретному мужчине и не может выдаваться без указания владельца. Чтобы разрешить эту проблему, ссылку на паспорт у мужчины сделаем опциональной, а ссылку на владельца у паспорта – константой. Также добавим паспорту конструктор, чтобы сразу определить его владельца. Таким образом, человек сможет существовать без паспорта, сможет его поменять или выкинуть, но паспорт может быть создан только с конкретным владельцем и никогда не может его сменить. Повторить все что на черном скрине и решить проблему соблюдая все правила!
class Man {
    var passport: Passport? // опциональная ссылка на паспорт
    
    deinit {
        print("мужчина удален из памяти")
    }
}

class Passport {
    let man: Man // константная ссылка на владельца
    
    init(man: Man) { // добавлен конструктор для установки связи между паспортом и владельцем
        self.man = man
        man.passport = self
    }
    
    deinit {
        print("паспорт удален из памяти")
    }
}

// Пример использования:
var john: Man? = Man()
var johnsPassport: Passport? = Passport(man: john!)
john = nil // паспорт все еще существует, но у него больше нет владельца
johnsPassport = nil
